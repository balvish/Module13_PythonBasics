import datetime

#User Input

user_input = input("Enter your goal with a deadline separated by colon\n")
input_list = user_input.split(":")

goal = input_list[0]
goal_date = input_list[1]
print(input_list)

#use the datetime library to format the provided date into the desired format for calculation
deadline_date = datetime.datetime.strptime(goal_date, "%d.%m.%Y")
current_date = datetime.datetime.today()
time_remaining = deadline_date - current_date

#Days Remaining until deadline
#print(time_remaining)

# Message to User

print(f"Time remaining for your goal: {goal} is {time_remaining.days} days")
